# Welcome!

This is my projects homepage.

## Public Repositories 

| Project                                                                    | Usage | Category | Language | Version | Dev. Status |
|:---------------------------------------------------------------------------|:------|:---------|:---------|:--------|:------------|
| [Chickenwire](https://github.com/cmarcbs7/chickenwire)                     | API   | Gaming   | Rust     | 0.0.0   | Active      |
| [One Finger Q-Learning](https://github.com/cmarcbs7/one-finger-q-learning) | AI    | Gaming   | Python   | 0.0.0   | Idle        |
| [Shenzhen Solitaire](https://github.com/cmarcbs7/shenzhen-solitaire)       | App   | Gaming   | Elm      | 0.9.0   | Idle        |
| [Tic-Tac-Yew](https://github.com/cmarcbs7/tic-tac-yew)                     | App   | Gaming   | Rust     | 0.0.0   | Active      | 

## My Language Interests

### Bash
_First Used: 2017_. BASH scripting is surprisingly fun. I appreciate the little nuances in the syntax.

### C
_First Used: 2017_. C was my University's primary teaching language. It's about as close to the metal as I'm comfortable working with. I enjoy that low-level stuff, but I don't think any sane developer should be using it in 2019.

### CSS/Sass
_First Used: 2017_. I'm very concerned when people cite 'variables' as a revolutionary new feature. Sass is fine though.

### Elixir 
_First Used: 2018_. I've been wanting to get more into it for a while. I really like the syntax just on an aesthetic level.

### Elm
_First Used: 2019_. I really appreciate what Elm is trying to do. I love working with dynamic sites, I hate working with JavaScript. Elm comes very close to bridging that gap for me, but falls just short. I'll use it for a lazy weekend project, but that's about it.

### Erlang
_First Used: 2018_. It's kinda cool. No strong feelings about it.

### Haskell
_First Used: 2019_. Still learning, but everyone keeps telling my how much I'll love it, so I guess I'll give it a good go.

### HTML/Slim
_First Used: 2017_. I appreciate the principle of HTML's design: the structure of the code matches the structure of the product. Slim 
is better though.

### JavaScript
_First Used: 2019_. Dear God who let this happen.

### Lua
_First Used: 2012_. Lua was my very first programming language. Someone wrote a mod for Minecraft called ComputerCraft which used 
Lua for the in-game terminals. Going back to the language later, I can't say it's my favorite, but I have a massive soft spot for it.

### Python (2 and 3)
_First Used: 2013_. Python is a language I often use casually, as well as the primary language of my University's machine learning
courses.

### R 
_First Used: 2018_. Eh.

### (Typed) Racket
_First Used: 2017_. For whatever reason, the University of Chicago's introductory computer science course insisted upon us using Typed 
Racket. While I developed an odd affection for the language, I'm not sure that's worth having parenthesis-related PTSD.

### Rust
_First Used: 2019_. Rust is my favorite programming language, hands down. I believe it's the definitive object-oriented language of the 
future, and so it's what I dedicate most of my time to learning. I could gush for pages, but this is meant to be a quick blurb, so...

### Wasm
_Unused_. Really really interested in this technology. Plan to get involved soon.

## About Me
I am a third year undergraduate in Neuroscience and Computer Science at the University of Chicago, inclined toward a career in machine learning applications toward healthcare and predictive prosthetic movement. Game design is my primary hobby, and I drink Dr Pepper with salted peanuts in it.
